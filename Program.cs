﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace sysdhelper
{
    class Program
    {
        /// <summary>  
        /// 关闭时的事件  
        /// </summary>  
        /// <param name="sender">对象</param>  
        /// <param name="e">参数</param>  
        protected static void CloseConsole(object sender, ConsoleCancelEventArgs e)
        {
            Environment.Exit(0);
        }

        [STAThread]
        static void Main(string[] args)
        {
            Console.CancelKeyPress += new ConsoleCancelEventHandler(CloseConsole);
            if (args.Length < 1 || args[0] == "?" || args[0] == "help")
            {
                Console.WriteLine("请输入参数：appPath -install[uninstall,start,stop] -user -group --name 服务名称 --display 显示名称 --description 描述");
            }
            else
            {
                string appPath = args[0];
                string cmd = "start";
                if (!GetNamedArguments(args, new string[] { "-install", "-uninstall", "-start", "-stop" }, ref cmd))
                    Console.WriteLine("未能获取操作指令");

                string svrName = typeof(Program).Assembly.GetName().Name;
                if (!GetNamedArguments(args, new string[] { "--name" }, ref svrName))
                    Console.WriteLine("未能获取服务名称 --name");

                Systemd sysd = new Systemd();
                bool cmdResult = false;
                if (cmd.EqualIgnoreCase("-install"))
                {
                    string displayName = string.Empty;
                    if (!GetNamedArguments(args, new string[] { "--display" }, ref cmd))
                        displayName = Path.GetFileNameWithoutExtension(appPath);

                    string desc = string.Empty;
                    if (!GetNamedArguments(args, new string[] { "--description" }, ref cmd))
                        desc = displayName + "服务描述";
                    cmdResult = sysd.Install(svrName, displayName, appPath, desc);
                }
                else if (cmd.EqualIgnoreCase("-uninstall"))
                {
                    cmdResult = sysd.Remove(svrName);
                }
                else if (cmd.EqualIgnoreCase("-start"))
                {
                    cmdResult = sysd.Start(svrName);
                }
                else if (cmd.EqualIgnoreCase("-stop"))
                {
                    cmdResult = sysd.Stop(svrName);
                }
                else
                {
                    Console.WriteLine("不识别的操作指令");
                }
                Console.WriteLine($"{cmd}操作指令 {cmdResult}");
            }
            Console.ReadKey();
        }

        public static bool GetNamedArguments(string[] args, string[] optionNames, ref string value)
        {
            value = string.Empty;

            if (optionNames == null)
                throw new ArgumentNullException(nameof(optionNames));

            if (args == null || args.Length < 2)
                return false;

            int idx = Array.FindIndex(args, s =>
            {
                for (int i = 0, j = optionNames.Length; i < j; i++)
                {
                    if (s.EqualIgnoreCase(optionNames[i]))
                        return true;
                }
                return false;
            });

            if (idx != -1)
            {
                if (args.Length >= idx + 1)
                    value = args[idx + 1];

                if (args.Length == idx || value.StartsWith("--") || value.StartsWith("-"))
                    value = args[idx];

                return true;
            }

            return false;
        }
    }

    /// <summary>服务主机</summary>
    public interface IHost
    {
        /// <summary>服务是否已安装</summary>
        /// <param name="serviceName">服务名</param>
        /// <returns></returns>
        Boolean IsInstalled(String serviceName);

        /// <summary>服务是否已启动</summary>
        /// <param name="serviceName">服务名</param>
        /// <returns></returns>
        Boolean IsRunning(String serviceName);

        /// <summary>安装服务</summary>
        /// <param name="serviceName">服务名</param>
        /// <param name="displayName">显示名</param>
        /// <param name="binPath">文件路径</param>
        /// <param name="description">描述信息</param>
        /// <returns></returns>
        Boolean Install(String serviceName, String displayName, String binPath, String description);

        /// <summary>卸载服务</summary>
        /// <param name="serviceName">服务名</param>
        /// <returns></returns>
        Boolean Remove(String serviceName);

        /// <summary>启动服务</summary>
        /// <param name="serviceName">服务名</param>
        /// <returns></returns>
        Boolean Start(String serviceName);

        /// <summary>停止服务</summary>
        /// <param name="serviceName">服务名</param>
        /// <returns></returns>
        Boolean Stop(String serviceName);

        /// <summary>重启服务</summary>
        /// <param name="serviceName">服务名</param>
        Boolean Restart(String serviceName);

        /// <summary>开始执行服务</summary>
        /// <param name="service"></param>
        void Run(ServiceBase service);
    }


    public abstract class ServiceBase
    {
        internal void StartLoop()
        {

        }

        internal void DoLoop()
        {

        }

        internal void StopLoop()
        {

        }
    }

    /// <summary>服务主机。用于管理控制服务</summary>
    public abstract class Host : IHost
    {
        /// <summary>服务是否已安装</summary>
        /// <param name="serviceName">服务名</param>
        /// <returns></returns>
        public virtual bool IsInstalled(string serviceName) => false;

        /// <summary>服务是否已启动</summary>
        /// <param name="serviceName">服务名</param>
        /// <returns></returns>
        public virtual Boolean IsRunning(String serviceName) => false;

        /// <summary>安装服务</summary>
        /// <param name="serviceName">服务名</param>
        /// <param name="displayName">显示名</param>
        /// <param name="binPath">文件路径</param>
        /// <param name="description">描述信息</param>
        /// <returns></returns>
        public virtual Boolean Install(String serviceName, String displayName, String binPath, String description) => false;

        /// <summary>卸载服务</summary>
        /// <param name="serviceName">服务名</param>
        /// <returns></returns>
        public virtual Boolean Remove(String serviceName) => false;

        /// <summary>启动服务</summary>
        /// <param name="serviceName">服务名</param>
        /// <returns></returns>
        public virtual Boolean Start(String serviceName) => false;

        /// <summary>停止服务</summary>
        /// <param name="serviceName">服务名</param>
        /// <returns></returns>
        public virtual Boolean Stop(String serviceName) => false;

        /// <summary>重启服务</summary>
        /// <param name="serviceName">服务名</param>
        public virtual Boolean Restart(String serviceName) => false;

        /// <summary>开始执行服务</summary>
        /// <param name="service"></param>
        public abstract void Run(ServiceBase service);
    }

    //https://github.com/NewLifeX/NewLife.Agent/blob/master/NewLife.Agent/Systemd.cs
    /// <summary>
    /// Linux守护进程
    /// </summary>
    public class Systemd : Host
    {
        private readonly String _path;
        private ServiceBase _service;

        /// <summary>用于执行服务的用户</summary>
        public String User { get; set; }

        /// <summary>用于执行服务的用户组</summary>
        public String Group { get; set; }

        /// <summary>实例化</summary>
        public Systemd()
        {
            var ps = new[] {
                "/etc/systemd/system",
                "/run/systemd/system",
                "/usr/lib/systemd/system",
                "/lib/systemd/system" };
            foreach (var p in ps)
            {
                if (Directory.Exists(p))
                {
                    _path = p;
                    break;
                }
            }
        }

        /// <summary>启动服务</summary>
        /// <param name="service"></param>
        public override void Run(ServiceBase service)
        {
            _service = service ?? throw new ArgumentNullException(nameof(service));

            try
            {
                // 启动初始化
                service.StartLoop();

                // 阻塞
                service.DoLoop();

                // 停止
                service.StopLoop();
            }
            catch (Exception ex)
            {
                XTrace.WriteException(ex);
            }
        }

        /// <summary>服务是否已安装</summary>
        /// <param name="serviceName">服务名</param>
        /// <returns></returns>
        public override Boolean IsInstalled(String serviceName)
        {
            var file = _path.CombinePath($"{serviceName}.service");
            return File.Exists(file);
        }

        /// <summary>服务是否已启动</summary>
        /// <param name="serviceName">服务名</param>
        /// <returns></returns>
        public override Boolean IsRunning(String serviceName)
        {
            var file = _path.CombinePath($"{serviceName}.service");
            if (!File.Exists(file)) return false;

            var str = Execute("systemctl", $"status {serviceName}", false);
            if (!str.IsNullOrEmpty() && str.Contains("running")) return true;

            return false;
        }

        /// <summary>安装服务</summary>
        /// <param name="serviceName">服务名</param>
        /// <param name="displayName">显示名</param>
        /// <param name="binPath">文件路径</param>
        /// <param name="description">描述信息</param>
        /// <returns></returns>
        public override Boolean Install(String serviceName, String displayName, String binPath, String description)
        {
            if (User.IsNullOrEmpty())
            {
                // 从命令行参数加载用户设置 -user
                var args = Environment.GetCommandLineArgs();
                if (args.Length >= 1)
                {
                    for (var i = 0; i < args.Length; i++)
                    {
                        if (args[i].EqualIgnoreCase("-user") && i + 1 < args.Length)
                        {
                            User = args[i + 1];
                            break;
                        }
                        if (args[i].EqualIgnoreCase("-group") && i + 1 < args.Length)
                        {
                            Group = args[i + 1];
                            break;
                        }
                    }
                    if (!User.IsNullOrEmpty() && Group.IsNullOrEmpty()) Group = User;
                }
            }

            return Install(_path, serviceName, displayName, binPath, description, User, Group);
        }

        /// <summary>安装服务</summary>
        /// <param name="systemdPath">systemd目录有</param>
        /// <param name="serviceName">服务名</param>
        /// <param name="displayName">显示名</param>
        /// <param name="binPath">文件路径</param>
        /// <param name="description">描述信息</param>
        /// <param name="user">用户</param>
        /// <param name="group">用户组</param>
        /// <returns></returns>
        public static bool Install(string systemdPath, String serviceName, String displayName, String binPath, String description, String user, String group)
        {
            XTrace.WriteLine("{0}.Install {1}, {2}, {3}, {4}", typeof(Systemd).Name, serviceName, displayName, binPath, description);
            var file = systemdPath.CombinePath($"{serviceName}.service");
            XTrace.WriteLine(file);

            //var asm = Assembly.GetEntryAssembly();
            var des = !displayName.IsNullOrEmpty() ? displayName : description;

            var sb = new StringBuilder();
            sb.AppendLine("[Unit]");
            sb.AppendLine($"Description={des}");

            sb.AppendLine();
            sb.AppendLine("[Service]");
            sb.AppendLine("Type=simple");
            //sb.AppendLine($"ExecStart=/usr/bin/dotnet {asm.Location}");
            sb.AppendLine($"ExecStart={binPath}");
            sb.AppendLine($"WorkingDirectory={".".GetFullPath()}");
            if (!user.IsNullOrEmpty()) sb.AppendLine($"User={user}");
            if (!group.IsNullOrEmpty()) sb.AppendLine($"Group={group}");
            sb.AppendLine("Restart=on-failure");

            sb.AppendLine();
            sb.AppendLine("[Install]");
            sb.AppendLine("WantedBy=multi-user.target");

            File.WriteAllText(file, sb.ToString());

            // sudo systemctl daemon-reload
            // sudo systemctl enable StarAgent
            // sudo systemctl start StarAgent

            Process.Start("systemctl", "daemon-reload");
            Process.Start("systemctl", $"enable {serviceName}");
            //Execute("systemctl", $"start {serviceName}");

            return true;
        }

        /// <summary>卸载服务</summary>
        /// <param name="serviceName">服务名</param>
        /// <returns></returns>
        public override bool Remove(String serviceName)
        {
            XTrace.WriteLine("{0}.Remove {1}", GetType().Name, serviceName);

            var file = _path.CombinePath($"{serviceName}.service");
            if (File.Exists(file)) File.Delete(file);

            return true;
        }

        /// <summary>启动服务</summary>
        /// <param name="serviceName">服务名</param>
        /// <returns></returns>
        public override bool Start(string serviceName) => Process.Start("systemctl", $"start {serviceName}") != null;

        /// <summary>停止服务</summary>
        /// <param name="serviceName">服务名</param>
        /// <returns></returns>
        public override Boolean Stop(String serviceName) => Process.Start("systemctl", $"stop {serviceName}") != null;

        /// <summary>重启服务</summary>
        /// <param name="serviceName">服务名</param>
        public override Boolean Restart(String serviceName) => Process.Start("systemctl", $"restart {serviceName}") != null;

        private static String Execute(String cmd, String arguments, Boolean writeLog = true)
        {
            if (writeLog) XTrace.WriteLine("{0} {1}", cmd, arguments);

            var psi = new ProcessStartInfo(cmd, arguments)
            {
                UseShellExecute = false,
                RedirectStandardOutput = true
            };
            var process = Process.Start(psi);
            if (!process.WaitForExit(3_000))
            {
                process.Kill();
                return null;
            }

            return process.StandardOutput.ReadToEnd();
        }
    }

    public static class XTrace
    {
        public static void WriteLine(string format, params object[] args)
        {
            Debug.WriteLine(format, args);
        }

        public static void WriteException(Exception ex)
        {
            Debug.WriteLine(ex.ToString());
        }

    }

    public static class HelpExtension
    {
        public static bool IsNullOrEmpty(this string str)
        {
            return string.IsNullOrEmpty(str);
        }

        public static string CombinePath(this string path, string tail)
        {
            return Path.Combine(path, tail);
        }

        public static bool EqualIgnoreCase(this string original, string cmpstr)
        {
            return original.Equals(cmpstr, StringComparison.InvariantCultureIgnoreCase);
        }

        public static string GetFullPath(this string path)
        {
            return Path.GetFullPath(path);
        }
    }
}
